<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    
<!DOCTYPE html>

<html>
<head>
<script src="<c:url value="/plugin/jquery-3.5.1.min.js"/>"></script>

<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<div>
	<form METHOD="post"  ACTION="userQuery" id="form">
		帳號:<input type="text" name="account" id="account"  ><br>
		名稱:<input type="text" name="username" id ="username">
		<button id="send">查詢</button>
	</form>
	
</div>

<form METHOD="post"  ACTION=userAction id="updateform">
	<input type="hidden" name="account" id="updateaccount"  >
	<input type="hidden" name="action" value="update"  >

</form>
<div id="grid">
		<table  border="1px" >
		<tr><th>動作</th><th>帳號</th><th>名子</th></tr>
			<c:forEach items="${userList}" var="item">
		   		<tr>
		   			<td><button onclick="update('${item.account}')">修改</button><button onclick="del('${item.account}')">刪除</button></td>			
					<td>${item.account}</td>			
					<td>${item.username}</td>			
					
				</tr>
			</c:forEach>
		
			
		</table>
</div>
	<button id="add" onclick="location.href='<c:url value="/userAction?action=add"/>'">新增</button>

<script>
$( document ).ready(function() {
	
	
	
	
	$("#send").on("click", function() {
		$("#form").submit();
				
	});
});
function del(userid){
	$.ajax({
		url:"<c:url value="/userUpdate?action=del"/>",
		type:"POST",
		data: {
			username : $("#username").val(),
			account: userid
		},
		success:function(data){
			$("#account").val(${searchKey.account});
			$("#username").val(${searchKey.username});

			$("#form").submit();

// 			$("#grid").html(data);
		}
	});
}

function update(userid){
	$("#updateaccount").val(userid);
	$("#updateform").submit();
}

function Add(){
	
}
</script>
</body>
</html>