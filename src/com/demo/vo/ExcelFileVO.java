package com.demo.vo;

public class ExcelFileVO {
	String fileOriName;
	String fileName;
	String filePath;

	public String getFileOriName() {
		return fileOriName;
	}

	public void setFileOriName(String fileOriName) {
		this.fileOriName = fileOriName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public ExcelFileVO() {
		super();
	}

	@Override
	public String toString() {
		return "ExcelFileVO [fileOriName=" + fileOriName + ", fileName=" + fileName + ", filePath=" + filePath + "]";
	}

}
