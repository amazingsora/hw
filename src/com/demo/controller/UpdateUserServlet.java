package com.demo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.demo.DTO.UserDTO;
import com.demo.service.UserService;
import com.demo.vo.UserVO;

//@WebServlet("/update")
@WebServlet(value = { "/userAction", "/userDetail", "/userUpdate" })

public class UpdateUserServlet extends HttpServlet {

	List<String> errList = null;

	UserService userService = new UserService();

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, res);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String url = req.getRequestURI().substring(req.getContextPath().length());

		errList = new ArrayList<String>();
		String action = "";
		action = req.getParameter("action");
		UserDTO user = new UserDTO();
		user.setAccount(req.getParameter("account"));
		user.setPassword(req.getParameter("password"));
		user.setUsername(req.getParameter("username"));

		if ("/userUpdate".equals(url)) {
			if (StringUtils.equals("del", action)) {

				userService.del(user.getAccount());
//				StringBuffer result = new StringBuffer(
//						"<table>" + "<tr>" + "    <td>動作</td>" + "    <td>帳號<td>" + "    <td>名<td>" + "</tr>");
//				for (Map<String, Object> data : userList) {
//					result.append("<tr>" + "<th><button onclick=\"update('" + data.get("account")
//							+ "')\">修改</button><button onclick=\"del('" + data.get("account") + "')\">刪除</button><th>"
//							+ "<th>" + data.get("account") + "<th>" + "<th>" + data.get("username") + "<th>");
//				}
//				result.append("</table>");
//				out.print(result.toString());
				RequestDispatcher successView = req.getRequestDispatcher("/WEB-INF/user/userQuery.jsp");
				successView.forward(req, res);
				return;
			}

			if (StringUtils.equals("add", action)) {

				vildvalue(user);
				if (errList.size() > 0) {
					req.setAttribute("err", errList.get(0));
					req.setAttribute("updateVO", user);
					RequestDispatcher successView = req.getRequestDispatcher("/WEB-INF/user/userDetail.jsp");
					successView.forward(req, res);

					return;
				}
				UserVO checkuser = new UserVO();
				checkuser.setAccount(req.getParameter("account"));
				List<Map<String, Object>> userList = userService.fetchAllUserList(checkuser);
				if (userList != null && userList.size() > 0) {
					req.setAttribute("err", "此帳號已存在");
					req.setAttribute("updateVO", user);
					RequestDispatcher successView = req.getRequestDispatcher("/WEB-INF/user/userDetail.jsp");
					successView.forward(req, res);
					return;
				} else {
					// 開始更新
					userService.insertUser(user);
					RequestDispatcher successView = req.getRequestDispatcher("/WEB-INF/user/userQuery.jsp");
					successView.forward(req, res);
					return;
				}

			}

			if (StringUtils.equals("update", action)) {
				if (StringUtils.isBlank(user.getUsername())) {
					errList.add("腳色姓名不得為空");
				}
				if (errList.size() > 0) {
					req.setAttribute("err", errList.get(0));
					req.setAttribute("action", action);
					req.setAttribute("updateVO", user);
					RequestDispatcher successView = req.getRequestDispatcher("/WEB-INF/user/userDetail.jsp");
					successView.forward(req, res);
					return;
				} else {
					userService.updateUser(user);
					RequestDispatcher successView = req.getRequestDispatcher("/WEB-INF/user/userQuery.jsp");
					successView.forward(req, res);
					return;
				}
			}
		}
		if ("/userAction".equals(url)) {
			if (StringUtils.equals("add", action)) {
			}

			if (StringUtils.equals("update", action)) {
				List<Map<String, Object>> userList = userService.fetchUserList(user);
				req.setAttribute("updateVO", userList.get(0));
				req.setAttribute("action", action);

			}
			RequestDispatcher successView = req.getRequestDispatcher("/WEB-INF/user/userDetail.jsp");

			successView.forward(req, res);

			return;
		} else if ("/userDetail".equals(url)) {
			RequestDispatcher failView = req.getRequestDispatcher("/WEB-INF/user/userQuery.jsp");
			failView.forward(req, res);

			return;

		}
	}

	private void vildvalue(UserDTO userDTO) {
		String account = userDTO.getAccount();
		String password = userDTO.getPassword();
		String username = userDTO.getUsername();
		if (StringUtils.isBlank(account) || StringUtils.isBlank(password) || StringUtils.isBlank(username)) {
			errList.add("請確實填寫");
		}
	}
}
