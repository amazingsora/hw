package com.demo.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.demo.service.ExcelFilesService;
import com.demo.vo.ExcelFileVO;
import com.demo.vo.excelVO;

/**
 * Servlet implementation class UploadFile
 */
@WebServlet("/uploadFile")
@MultipartConfig
public class UploadFileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	ExcelFilesService excelFilesService = new ExcelFilesService();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UploadFileServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = (String) request.getParameter("action");
		if ("page".equals(action)) {
			RequestDispatcher sucessView = request.getRequestDispatcher("/WEB-INF/upload/uploadExcel.jsp");
			sucessView.forward(request, response);
			return;
		}
		List<excelVO> list = new LinkedList<>();

		Part file = request.getPart("file");
		FileOutputStream out = null;
		InputStream in = null;
		try {
			if (file != null) {
				SimpleDateFormat sdFormat = new SimpleDateFormat("yyyyMMddHHmmss");
				String fileName = sdFormat.format(new Date());
				String filePath = "D:/temp/" + fileName + ".xlsx";

				in = file.getInputStream();
				out = new FileOutputStream(filePath); // 定義 OutputStram 輸出位置
				Workbook workbook = WorkbookFactory.create(in);

				byte[] buffer = new byte[1024];
				int length = -1;
				while ((length = in.read(buffer)) != -1) {
					out.write(buffer, 0, length); // 檔案寫入
				}

				ExcelFileVO excelFileVO = new ExcelFileVO();

				excelFileVO.setFileName(fileName + ".xlsx");
				excelFileVO.setFileOriName(file.getSubmittedFileName());
				excelFileVO.setFilePath(filePath);

				System.out.println("excelFileVO === " + excelFileVO);
				excelFilesService.insertExcelFile(excelFileVO);
				Sheet sheet = workbook.getSheetAt(0);
				int rowCount = sheet.getLastRowNum();
				for (int i = 1; i < rowCount; i++) {
					Row row = sheet.getRow(i);
					excelVO vo = new excelVO();
					vo.setCurrency(getValue(row.getCell(0)));

					vo.setBuyRate(getValue(row.getCell(1)));
					vo.setBuyCash(getValue(row.getCell(2)));
					vo.setBuyInTime(getValue(row.getCell(3)));
					vo.setBuyDay10(getValue(row.getCell(4)));
					vo.setBuyDay30(getValue(row.getCell(5)));
					vo.setBuyDay60(getValue(row.getCell(6)));
					vo.setBuyDay90(getValue(row.getCell(7)));
					vo.setBuyDay120(getValue(row.getCell(8)));
					vo.setBuyDay150(getValue(row.getCell(9)));
					vo.setBuyDay180(getValue(row.getCell(10)));

					vo.setSellRate(getValue(row.getCell(11)));
					vo.setSellCash(getValue(row.getCell(12)));
					vo.setSellInTime(getValue(row.getCell(13)));
					vo.setSellDay10(getValue(row.getCell(14)));
					vo.setSellDay30(getValue(row.getCell(15)));
					vo.setSellDay60(getValue(row.getCell(16)));
					vo.setSellDay90(getValue(row.getCell(17)));
					vo.setSellDay120(getValue(row.getCell(18)));
					vo.setSellDay150(getValue(row.getCell(19)));
					vo.setSellDay180(getValue(row.getCell(20)));
					System.out.println(vo);
					list.add(vo);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();

			}
			if (out != null) {
				out.close();

			}

		}
		System.out.println("LIST ===" + list);
		RequestDispatcher sucessView = request.getRequestDispatcher("/WEB-INF/upload/uploadExcel.jsp");
		request.setAttribute("excelList", list);
		sucessView.forward(request, response);
		return;

	}

	private String getValue(Cell cell) {
		if (cell.getCellType() == CellType.BOOLEAN) {
			// 餈��憿����
			return String.valueOf(cell.getBooleanCellValue());
		} else if (cell.getCellType() == CellType.NUMERIC) {
			// 餈��潮�����
			return String.valueOf(cell.getNumericCellValue());
		} else {
			// 餈��泵銝脤�����
			return String.valueOf(cell.getStringCellValue());
		}
	}
}
