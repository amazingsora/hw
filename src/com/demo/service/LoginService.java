package com.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.demo.DTO.UserDTO;
import com.demo.dao.UserDao;
import com.demo.dao.UserDaoImpl;
import com.demo.vo.UserVO;

public class LoginService {
	UserDao dao = new UserDaoImpl();

	public List<String> validLogin(UserDTO userDTO) {
		List<String> err = new ArrayList<String>();
		if (StringUtils.isBlank(userDTO.getAccount()) && StringUtils.isBlank(userDTO.getPassword())) {
			err.add("請確實填寫");
			return err;
		}
		UserVO user = new UserVO();
		user.setAccount(userDTO.getAccount());
		user.setPassword(userDTO.getPassword());
		user = dao.LoginUser(user);
		if (user == null) {
			err.add("帳號密碼異常 查無使用者");
		} else {
			if (!"Y".equals(user.getStatus().trim())) {
				err.add(user.getAccount() + "尚未啟用");
			}
		}
		return err;
	}

}
