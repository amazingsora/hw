package com.demo.service;

import java.util.List;
import java.util.Map;

import com.demo.DTO.UserDTO;
import com.demo.dao.UserDao;
import com.demo.dao.UserDaoImpl;
import com.demo.vo.UserVO;

public class UserService {
	UserDao dao = new UserDaoImpl();

	public List<Map<String, Object>> fetchUserList(UserDTO userDTO) {
		UserVO userVO =new UserVO();
		userVO.setAccount(userDTO.getAccount());
		userVO.setUsername(userDTO.getUsername());
		userVO.setPassword(userDTO.getPassword());

		return dao.findUserList(userVO);
	}
	public List<Map<String, Object>> fetchAllUserList(UserVO userVo) {

		return dao.findUserList(userVo,null);
	}
	public void del(String account) {
		dao.changeStatus(account, "N");
	}

	public void insertUser(UserDTO userDTO) {
		UserVO userVO =new UserVO();
		userVO.setAccount(userDTO.getAccount());
		userVO.setUsername(userDTO.getUsername());
		userVO.setPassword(userDTO.getPassword());
		userVO.setStatus("Y");
		userVO.setNote("手動新增");
		dao.insert(userVO);
	}

	public void updateUser(UserDTO userDTO) {
		dao.update(userDTO.getAccount(), userDTO.getUsername());
	}
}
