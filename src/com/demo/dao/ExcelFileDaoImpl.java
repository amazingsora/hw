package com.demo.dao;

import com.demo.common.dao.BaseDao;
import com.demo.vo.ExcelFileVO;

public class ExcelFileDaoImpl extends BaseDao implements ExcelFileDao  {

	@Override
	public void insert(ExcelFileVO excelFileVO) {
		String sql = "INSERT INTO EXCELFILE (FILEPATH,FILEORENAME,FILENAME)   " + "VALUES (?, ?, ?)";
		executeSql(sql, excelFileVO.getFilePath(),excelFileVO.getFileOriName(),excelFileVO.getFileName());
		
	}

}
